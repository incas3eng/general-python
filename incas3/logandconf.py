# import of standard packages
import configparser
import io
import logging, logging.config
import os

# Metadata
__authors__ = "Dirkjan Krijnders"
__contact__ = "Dirkjan Krijnders <dirkjankrijnders@incas3.eu>"
__copyright__  = "Stichting INCAS3"
__license__ = "INCAS3 internal use only"
__date__ = "2016-04-14"
__version__ = "0.1.0"

def logAndConf(name):
	# Read the contig file.
	Config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation());
	Config.read(os.path.join('/etc/incas3/', name + '.ini'));
	Config.read(os.path.join(os.path.expanduser("~"), "." + name + '.ini'));
	Config.read(name + '.ini');

	# Configure and create the logging, can be done from /etc/incas3/<name>_logging.conf
	logConfFile = os.path.join('/etc/incas3/', name + '_logging.conf');
	if os.path.exists(logConfFile):
		logging.config.fileConfig(logConfFile)
		
	logger = logging.getLogger(name)

	logger.info("Starting Distrisense tools");
	logger.info("Version {} Copyright {}".format(__version__, __copyright__));
	
	s = io.StringIO()
	Config.write(s)
	logger.debug("Effective configuration:\n{}".format(s.getvalue()));
	s.close()
	
	return Config, logger