#!/usr/bin/env python3

from setuptools import setup

setup(name='incas3-python',
      version='1.0',
      description='General python code for incas3 projects',
      author='Dirkjan Krijnders',
      author_email='dirkjankrijnders@incas3.eu',
      url='https://bitbucket.org/incas3eng/general-python/',
      packages=['incas3'],
     )
